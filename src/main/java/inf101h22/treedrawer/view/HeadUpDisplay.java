package inf101h22.treedrawer.view;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;

import inf101h22.observable.ObservableValue;

/**
 * Draws the head-up display displaying a message
 */
public class HeadUpDisplay extends JComponent {


    /** Creates a new HeadUpDispaly panel for the TreeDrawer application. */
    public HeadUpDisplay(ObservableValue<String> observableValue) {
        JLabel message = new JLabel(observableValue.getValue());
        observableValue.addObserver(() -> {
            message.setText(observableValue.getValue());
        });

        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(message);
        this.add(Box.createHorizontalGlue());
    }


}
