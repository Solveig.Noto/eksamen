package inf101h22.treedrawer.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import inf101h22.list.ObservableList;
import inf101h22.treedrawer.model.Node;
import inf101h22.treedrawer.model.ReadableTreeCanvas;

public class TreeCanvasView extends JPanel {

    // Constants
    private static final int DEF_WIDTH = 500;
    private static final int DEF_HEIGHT = 300;
    private static final int RADIUS = 10;
    private static final Color NODE_COLOR = Color.RED;
    private static final Color EDGE_COLOR = Color.BLACK;
    private static final Color FRAME_COLOR = Color.BLACK;

    // Field variables
    private ObservableList<Node> nodes;

    /** Creates a new panel which displays a ReadableTreeCanvas model */
    public TreeCanvasView(ReadableTreeCanvas model) {
        this.nodes = model.getNodes();
        this.nodes.addObserver(this::repaint);
        this.setPreferredSize(new Dimension(DEF_WIDTH, DEF_HEIGHT));
    }

    @Override
    protected void paintComponent(Graphics canvas) {
        super.paintComponent(canvas);
        drawFrame(canvas);
        drawEdges(canvas);
        drawNodes(canvas);
    }

    /** Draws a frame around the canvas. */
    private void drawFrame(Graphics canvas) {
        canvas.setColor(FRAME_COLOR);
        canvas.drawRect(0, 0, this.getWidth()-1, this.getHeight()-1);
    }

    /** Draws the edges of the tree on the canvas. */
    private void drawEdges(Graphics canvas) {
        canvas.setColor(EDGE_COLOR);
        for (Node node : this.nodes) {
            if (node.neighbor != null) {
                int x = (int) (node.x * this.getWidth());
                int y = (int) (node.y * this.getHeight());
                int nx = (int) (node.neighbor.x * this.getWidth());
                int ny = (int) (node.neighbor.y * this.getHeight());
                canvas.drawLine(x, y, nx, ny);
            }
        }
    }

    /** Draws the dots of the tree on the canvas. */
    private void drawNodes(Graphics canvas) {
        canvas.setColor(NODE_COLOR);
        for (Node node : this.nodes) {
            int x = (int) (node.x * this.getWidth());
            int y = (int) (node.y * this.getHeight());
            canvas.fillOval(x - RADIUS, y - RADIUS, 2 * RADIUS, 2 * RADIUS);
        }
    }
}
