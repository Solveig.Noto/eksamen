package inf101h22.observable;

import java.util.ArrayList;
import java.util.List;

/**
 * An ObservableCore provides the core functionality for making a class
 * Observable. This essentially means making callbacks to observers.
 * It is the responsibility of the specific class implementing Observable
 * to make a call to notifyObservers() on the ObservableCore every time
 * the observed state mutates.
 * 
 * <p>
 * The class can be used either through inheritance or through
 * composition. Inheritance provides a smaller code footprint, whereas
 * composition allows the Observable class to inherit other classes.
 * 
 * <p>
 * Example of use through composition (notice how notifyObservers is
 * called in setValue):
 * 
 * <pre>{@code
 *     public class MyObservableInt implements Observable {
 * 
 *         private int value;
 *         private ObservableCore observableCore = new ObservableCore();
 * 
 *         public MyObservableInt(int initialValue) {
 *             this.value = initialValue;
 *         }
 * 
 *         public void setValue(int newValue) {
 *             if (this.value != newValue) {
 *                 this.value = newValue;
 *                 observableCore.notifyObservers();
 *             }
 *         }
 * 
 *         public int getValue() {
 *             return this.value;
 *         }
 * 
 *         &#64;Override
 *         public void addObserver(Observer observer) {
 *             this.observableCore.addObserver(observer);
 *         }
 * 
 *         &#64;Override
 *         public boolean removeObserver(Observer observer) {
 *             return this.observableCore.removeObserver(observer);
 *         }
 *     }
 * }</pre>
 * 
 * 
 * Example using inheritance:
 * 
 * 
 * <pre>{@code
 *     public class MyObservableInt extends ObservableCore {
 *         private int value;
 *
 *         public MyObservableInt(int initialValue) {
 *             this.value = initialValue;
 *         }
 * 
 *         public void setValue(int newValue) {
 *             if (this.value != newValue) {
 *                 this.value = newValue;
 *                 this.notifyObservers();
 *             }
 *         }
 * 
 *         public int getValue() {
 *             return this.value;
 *         }
 *     }
 * }</pre>
 */
public class ObservableCore implements Observable {

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public boolean removeObserver(Observer observer) {
        return this.observers.remove(observer);
    }

    public void notifyObservers() {
        for (Observer obs : this.observers) {
            obs.update();
        }
    }

}
